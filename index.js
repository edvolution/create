#!/usr/bin/env node

const { run } = require("./lib/env");
const { getProject } = require("./lib/prompt");

async function bootstrap() {
  try {
    const generator = await getProject();
    await run(generator);
  } catch (err) {
    throw err;
  }
}

bootstrap();
