# Edvolution Initializer

> Initializer for npm projects

## Usage

```sh
# Create new project with Edvolution presets
npm init @edvolution
```

## Deployment

Updates to this library can be deployed to the public namespace.

```sh
npm publish --access public
```

## Dependencies

- [node ^10.3](https://nodejs.org)
- [npm ^6.1](https://npmjs.com)
