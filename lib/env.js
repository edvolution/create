const path = require("path");
const yeoman = require("yeoman-environment");
const { forEach } = require("lodash");

// Initialize Yeoman
const env = yeoman.createEnv();

// List of envs and their respective directory
const envs = {
  default: "app",
  react: "react"
};

// Register envs with Yeoman
forEach(envs, (directory, namespace) => {
  const generator = path.resolve(__dirname, `../generators/${directory}`);
  env.register(require.resolve(generator), namespace);
});

// Promisify Yeoman's run method
const run = generator =>
  new Promise((resolve, reject) => {
    env.run(generator, err => (err ? reject(err) : resolve()));
  });

module.exports = { env, run };
