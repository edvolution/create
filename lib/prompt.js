const { prompt } = require("inquirer");

// Return generator name from select prompt. This is used for bootstrapping
// Yeoman, as such we use Enquirer manually rather than the Yeoman instance
const getProject = async () => {
  const { generator } = await prompt({
    type: "list",
    name: "generator",
    message: "What kind of project are you working on?",
    initial: 0,
    choices: [
      { name: "Node", short: "Node", value: "default" },
      { name: "Babel", short: "Babel", value: "babel", disabled: true },
      { name: "TypeScript", short: "TS", value: "typescript", disabled: true },
      { name: "React", short: "React", value: "react", disabled: true }
    ]
  });
  return generator;
};

// Metadata
async function getDetails() {
  // Basic metadata
  const metadata = await this.prompt([
    {
      type: "input",
      name: "name",
      message: "Your project name"
    },
    {
      type: "input",
      name: "description",
      message: "A description for the project"
    }
  ]);

  // Contributor data
  const buildContributors = async () => {
    const input = await this.prompt([
      {
        type: "input",
        name: "user",
        message: "Your name"
      }
    ]);

    return input.user && input.user.length > 0
      ? {
          ...input,
          ...(await this.prompt([
            {
              type: "input",
              name: "email",
              message: "Your email address"
            }
          ]))
        }
      : { user: "Edvolution Labs", email: "labs@edvolution.io" };
  };
  const contributors = await buildContributors();

  // Release and licenses
  const buildLicense = async () => {
    const input = await this.prompt({
      type: "confirm",
      name: "private",
      message: "Is the project private?"
    });

    return input.private
      ? { private: true, license: "UNLICENSED" }
      : {
          ...input,
          ...(await this.prompt({
            type: "list",
            name: "license",
            message: "What kind of license do you want to use?",
            choices: [
              { name: "MIT", short: "MIT", value: "MIT" },
              { name: "ISC", short: "ISC", value: "ISC" },
              { name: "GPLv3", short: "GPLv3", value: "LGPL-3.0-only" },
              { name: "Apache 2.0", short: "Apache 2.0", value: "Apache-2.0" }
            ]
          }))
        };
  };
  const license = await buildLicense();

  const engine = await this.prompt({
    type: "list",
    name: "engine",
    message: "What version of Node do you want to use?",
    default: 1,
    choices: [
      { name: "v8.x (lts/carbon)", short: "8.x", value: 8 },
      { name: "v10.x (lts/dubnium)", short: "10.x", value: 10 }
    ]
  });

  return {
    ...metadata,
    ...contributors,
    ...license,
    ...engine
  };
}

module.exports = { getProject, getDetails };
