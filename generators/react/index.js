const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  method1() {
    this.log("react method 1 just ran");
  }

  method2() {
    this.log("react method 2 just ran");
  }
};
