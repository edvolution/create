const Generator = require("yeoman-generator");
const { getDetails } = require("../../lib/prompt");

module.exports = class extends Generator {
  async prompting() {
    const metadata = { author: "Edvolution Labs", version: "1.0.0" };
    const answers = await getDetails.apply(this);
    this.config.set("node", { ...metadata, ...answers });
  }

  _copy(src, dest = false) {
    this.fs.copyTpl(
      this.templatePath(src),
      this.destinationPath(dest || src),
      this.config.get("node")
    );
  }

  writing() {
    this._copy("README.md");
    this._copy("package.json");
    this._copy("index.js");
    this._copy("gitignore", ".gitignore");
    this._copy("nvmrc", ".nvmrc");
    this._copy("eslintrc", ".eslintrc");
  }

  install() {
    this.npmInstall();
  }
};
