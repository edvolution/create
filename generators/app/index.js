const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  initializing() {
    this.composeWith(require.resolve("../node"));
  }

  configuring() {
    this.answers = this.config.get("node");
  }

  method1() {
    this.log(this.answers);
  }
};
